-- =============== --
-- christmas nodes --
-- =============== --

-- decorations --

minetest.register_craft({
	output = "christmas_craft:christmas_lights 4",
	recipe = {
		{"farming:string","default:mese_crystal", "farming:string"},
		{"default:glass","default:glass", "default:glass"},
	}
})

minetest.register_craft({
	output = "christmas_craft:christmas_wall_lights 4",
	recipe = {
		{"","default:mese_crystal", ""},
		{"default:glass","default:glass", "default:glass"},
	}
})

minetest.register_craft({
	output = "christmas_craft:christmas_leaves 4",
	recipe = {
		{"default:leaves","default:leaves"},
		{"default:leaves","default:leaves"},
	}
})

minetest.register_craft({
	output = "christmas_craft:christmas_star ",
	recipe = {
		{"","default:gold_ingot",""},
		{"default:gold_ingot","default:gold_ingot","default:gold_ingot"},
		{"default:gold_ingot","","default:gold_ingot"},
	}
})

minetest.register_craft({
	output = "christmas_craft:christmas_wreath ",
	recipe = {
		{"christmas_craft:christmas_leaves","christmas_craft:christmas_leaves","christmas_craft:christmas_leaves"},
		{"christmas_craft:christmas_leaves","","christmas_craft:christmas_leaves"},
		{"christmas_craft:christmas_leaves","christmas_craft:red_ribbon","christmas_craft:christmas_leaves"},
	}
})

-- snow node --

minetest.register_craft({
	output = "christmas_craft:snow_slab 3",
	recipe = {
		{"default:snow","default:snow","default:snow"},
  }
})

minetest.register_craft({
	output = "christmas_craft:snow_steps 3",
	recipe = {
    {"default:snow","",""},
    {"default:snow","default:snow",""},
		{"default:snow","default:snow","default:snow"},
  }
})

minetest.register_craft({
	output = "christmas_craft:snow_steps_1 3",
	recipe = {
    {"christmas_craft:snow_steps","christmas_craft:snow_steps",""},
    {"christmas_craft:snow_steps","",""},
  }
})

minetest.register_craft({
	output = "christmas_craft:snow_steps_2",
	recipe = {
    {"christmas_craft:snow_steps"},
  }
})

minetest.register_craft({
	output = "christmas_craft:snowman",
	recipe = {
		{"default:coal_lump","default:snow","default:coal_lump"},
		{"default:snow","christmas_craft:snowball","default:snow"},
		{"default:coal_lump","default:coal_lump","default:coal_lump"},
	}
})

-- food --

minetest.register_craft({
	output = "christmas_craft:ginger_mix",
	recipe = {
		{"christmas_craft:sugar","christmas_craft:sugar"},
		{"farming:flour","farming:flour"},
	}
})

minetest.register_craft({
	type = "cooking",
	output = "christmas_craft:ginger_bread_man",
	recipe = "christmas_craft:ginger_mix",
})

minetest.register_craft({
	output = "christmas_craft:christmas_pudding_mix",
	recipe = {
		{"christmas_craft:sugar","christmas_craft:sugar","christmas_craft:sugar"},
		{"default:apple","farming:flour","default:apple"},
		{"farming:flour","default:apple","farming:flour"},
	}
})

minetest.register_craft({
	type = "cooking",
	output = "christmas_craft:christmas_pudding",
	recipe = "christmas_craft:christmas_pudding_mix",
})

minetest.register_craft({
  type = "cooking",
  output = "christmas_craft:sugar",
  recipe = "group:flower",
})

-- candy cain

minetest.register_craft({
	output = "christmas_craft:candy_cane",
	recipe = {
		{"","christmas_craft:sugar","christmas_craft:sugar"},
		{"","christmas_craft:sugar",""},
		{"christmas_craft:sugar","",""},
	}
})


minetest.register_craft({
	output = "christmas_craft:candy_cane_node",
	recipe = {
		{"christmas_craft:candy_cane","christmas_craft:candy_cane","christmas_craft:candy_cane"},
		{"christmas_craft:candy_cane","","christmas_craft:candy_cane"},
		{"christmas_craft:candy_cane","christmas_craft:candy_cane","christmas_craft:candy_cane"},
	}
})

minetest.register_craft({
	output = "christmas_craft:candy_cane_tree",
	recipe = {
		{"christmas_craft:candy_cane","christmas_craft:candy_cane","christmas_craft:candy_cane"},
		{"christmas_craft:candy_cane","group:tree","christmas_craft:candy_cane"},
	   {"christmas_craft:candy_cane","christmas_craft:candy_cane","christmas_craft:candy_cane"},
	}
})

-- ribbon craft --

minetest.register_craft({
	type = "shapeless",
	output = 'christmas_craft:red_ribbon',
	recipe = {'dye:red','farming:string'},
})

-- wish list craft --

minetest.register_craft({
	type = "shapeless",
	output = 'christmas_craft:wish_list',
	recipe = {'default:stick','default:mese_crystal','default:paper','dye:black'},
})

-- present box --

minetest.register_craft({
	output = "christmas_craft:present_box",
	recipe = {
		{"default:paper","default:paper", "default:paper"},
		{"default:paper","christmas_craft:wish_list", "default:paper"},
		{"default:paper","default:paper", "default:paper"},
	}
})
