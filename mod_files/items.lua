minetest.register_craftitem("christmas_craft:red_ribbon", {
description = "Red Ribbon",
inventory_image = "christmas_craft_red_ribbon.png",
stack_max = 99,
liquids_pointable = false,
})

-- wish list --

minetest.register_craftitem("christmas_craft:wish_list", {
description = "Wish list",
inventory_image = "christmas_craft_which_list.png",
stack_max = 99,
liquids_pointable = false,
})

minetest.register_craftitem("christmas_craft:christmas_pudding_mix", {
	description = "Christmas Pudding Mix",
	inventory_image = "christmas_pud-mix.png",
})

minetest.register_craftitem("christmas_craft:ginger_mix", {
	description = "Christmas Ginger Mix",
	inventory_image = "christmas_ginger-mix.png",
})

minetest.register_craftitem("christmas_craft:sugar", {
	description = "Christmas sugar",
	inventory_image = "christmas_sugar.png",
})

minetest.register_node("christmas_craft:ginger_bread_man", {
	description = "Ginger Bread Man",
	drawtype = "signlike",
	walkable = false,
	tiles =
	{name="ginger_bread_man.png", animation={type="vertical_frames", aspect_w=32, aspect_h=32, length=3.0}},
	inventory_image =  "ginger_bread_man.png",
	wield_image = "ginger_bread_man.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	selection_box = {
	type = "wallmounted",
	},
  groups = {oddly_breakable_by_hand = 3},
  on_use = minetest.item_eat(4),
})
