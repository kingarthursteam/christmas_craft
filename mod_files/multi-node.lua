-- ===================== --
-- Christmas Craft Nodes --
-- ===================== --

print("scaffolding: Loading 'functions.lua'")

-- ============ --
-- Node Present --
-- ============ --

-- Present boxs --

minetest.register_node("christmas_craft:present_box", {
  description = "Present Box",
  tiles = {"christmas_present_box.png"},
  is_ground_content = true,
  paramtype = "light",
  groups = {crumbly=3},
  sounds = default.node_sound_sand_defaults(),
})

-- coloured Present --

local function register_present(name, description, colorCode, dye)

minetest.register_node("christmas_craft:Christmas_present_"..name, {
  description = description .." Christmas Present",
  tiles = {
    "christmas_present.png^[colorize:#" ..colorCode.. "^christmas_bow_top.png",
    "christmas_present.png^[colorize:#" ..colorCode.. "^christmas_bow_bottom.png",
    "christmas_present.png^[colorize:#" ..colorCode.. "^christmas_bow_side.png"},
  is_ground_content = true,
  groups = {crumbly=3},
  drop = {
    max_items = 1, min_items = 1, items = {
      {items = {'default:bookshelf'},	rarity = 90,},
      {items = {'default:pick_mese'},	rarity = 80,},
      {items = {'default:shovel_steel'},	rarity = 90,},
      {items = {'default:axe_steel'},	rarity = 90,},
      {items = {'default:pick_steel'},	rarity = 90,},
      {items = {'default:sign_wall'},	rarity = 80,},
      {items = {'default:chest'},	rarity = 80,},
      {items = {'default:furnace'},	rarity = 80,},
      {items = {'default:steelblock'},	rarity = 80,},
      {items = {'default:coal_lump'},	rarity = 80,},
      {items = {'default:pick_diamond'},	rarity = 75,},
      {items = {'default:shovel_diamond'},	rarity = 75,},
      {items = {'default:axe_diamond'},	rarity = 75,},
      {items = {'default:diamondblock'},	rarity = 75},
      {items = {'fake_fire:flint_and_steel'},	rarity = 90,},
      {items = {'default:chest_locked'},	rarity = 80,},
      {items = {'default:brick'},	rarity = 80,},
      {items = {'default:dirt_with_grass'}, rarity = 80,},
    }},
  sounds = default.node_sound_dirt_defaults({
    footstep = {name="default_grass_footstep", gain=0.4},
  }),
})

minetest.register_craft({
	output = "christmas_craft:christmas_present"..name ,
	recipe = {
		{"christmas_craft:paper_".. name,"christmas_craft:red_ribbon", "christmas_craft:paper_".. name},
		{"christmas_craft:paper_".. name,"christmas_craft:present_box", "christmas_craft:paper_".. name},
		{"christmas_craft:paper_".. name,"christmas_craft:red_ribbon", "christmas_craft:paper_".. name},
	}
})

end

-- ============ --
-- Node Baubles --
-- ============ --

-- Glass Baunles --

minetest.register_node("christmas_craft:glass_bauble",{
  description = "Bauble",
  drawtype = "nodebox",
  tiles = {
    "christmas_baubles-top.png^christmas_baubles_top.png",
    "christmas_baubles-top.png^christmas_baubles_side.png",
    "christmas_baubles-side.png^christmas_baubles_side.png",
  },
  is_ground_content = true,
  paramtype = "light",
  use_texture_alpha = "blend",
  groups = {crumbly=3},
	sounds = default.node_sound_glass_defaults(),
  node_box = {
    type = "fixed",
    fixed = {
      {-0.0625, 0.375, -0.0625, 0.0625, 0.5, 0.0625}, -- NodeBox1
      {-0.25, -0.0625, -0.25, 0.25, 0.4375, 0.25}, -- NodeBox2
    }
  },
  selection_box = {
    type = "fixed",
    fixed = {
      {-0.0625, 0, -0.0625, 0.0625, 0.5, 0.0625}, -- NodeBox1
      {-0.25, -0.0625, -0.25, 0.25, 0.4375, 0.25}, -- NodeBox2
    },
  },
})

minetest.register_craft({
	output = "christmas_craft:glass_bauble 8",
	recipe = {
		{"default:glass","default:gold_ingot", "default:glass"},
		{"default:glass","", "default:glass"},
		{"default:glass","default:glass", "default:glass"},
	}
})

-- Colour Baunles --

local function register_baubles(name, description, colorCode, dye)

  minetest.register_node("christmas_craft:" .. name .. "_baubles",{
  	description = description.. " Baubles",
  	drawtype = "nodebox",
  	tiles = {
      "christmas_baubles.png^[colorize:#" ..colorCode.. "^christmas_baubles_top.png",
      "christmas_baubles.png^[colorize:#" ..colorCode.. "^christmas_baubles_side.png",
      "christmas_baubles.png^[colorize:#" ..colorCode.. "^christmas_baubles_side.png",
    },
  	is_ground_content = true,
  	paramtype = "light",
  	groups = {crumbly=3},
  	sounds = default.node_sound_glass_defaults(),
  	node_box = {
  		type = "fixed",
      fixed = {
  			{-0.0625, 0.375, -0.0625, 0.0625, 0.5, 0.0625}, -- NodeBox1
  			{-0.25, -0.0625, -0.25, 0.25, 0.4375, 0.25}, -- NodeBox2
  		}
  	},
  	selection_box = {
  		type = "fixed",
      fixed = {
  			{-0.0625, 0, -0.0625, 0.0625, 0.5, 0.0625}, -- NodeBox1
  			{-0.25, -0.0625, -0.25, 0.25, 0.4375, 0.25}, -- NodeBox2
  		},
  	},
  })

  minetest.register_craft({
  	output = "christmas_craft:" .. name .. "_baubles 8",
  	recipe = {
  		{"default:glass","default:gold_ingot", "default:glass"},
  		{"default:glass","dye:"..dye, "default:glass"},
  		{"default:glass","default:glass", "default:glass"},
  	}
  })
end

local function register_paper(name, description, colorCode, dye)

  minetest.register_craftitem("christmas_craft:paper_".. name, {
		description = description .."paper",
		inventory_image = "default_paper.png^[colorize:#" .. colorCode,
		stack_max = 99,
		liquids_pointable = false,
	})

  minetest.register_craft({
  	type = "shapeless",
  	output = 'christmas_craft:paper_'.. name,
  	recipe = {'dye:'..dye,'default:paper'},
  })

end

colours = {
	-- RGB Prime Colours --
	{name="red", code="FF000099", description="Red", dye="red"},
	{name="green", code="00FF0099", description="Green", dye="green"},
	{name="blue", code="0000FF99", description="Blue", dye="blue"},
	-- RGB Secondary Colour --
	{name="yellow", code="FFFF0099", description="Yellow", dye="yellow"},
	{name="magenta", code="FF00FF99", description="Magenta", dye="magenta"},
	{name="cyan", code="00FFFF99", description="Cyan", dye="cyan"},
	-- RGB Other Colours --
	{name="orange", code="E5940099", description="Orange", dye="orange"},
	{name="darkgreen", code="004C0099", description="Dark Green", dye="dark_green"},
	{name="violet", code="80008099", description="Violet", dye="purple"},
	{name="pink", code="FFC0CB99", description="Pink", dye="pink"},
	{name="brown", code="732c0b99", description="Brown", dye="brown"},
	-- MonoChrome --
	{name="white", code="FFFFFF99", description="White", dye="white"},
	{name="grey", code="80808099", description="Grey", dye="grey"},
	{name="darkgrey", code="14141499", description="Dark Grey", dye="dark_grey"}
}

for i,colour in ipairs(colours) do
	register_present(colour.name, colour.description, colour.code, colour.dye)
  register_baubles(colour.name, colour.description, colour.code, colour.dye)
  register_paper(colour.name, colour.description, colour.code, colour.dye)
	-- register whatever here
end
