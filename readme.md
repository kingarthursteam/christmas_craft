CHRISTMAS CRAFT MOD FOR MINETEST
==================================
version 1.0

**This Mod has been tested my myself and needs more testing for bugs**
(though the likely bugs are crafting errors)

ABOUT THE MOD
------------------------------------------
Bring the joy of Christmas to Minetest. This mod adds 
Christmas related things such as presents, baubles , 
decorations and more, also the mod can also allows you 
to cover the grass with snow, and make things such as 
snowmen. You can even have a snowball fight! 

WHATS NEW?
------------------------------------------


 *   New food for the mod
 *   New snow nodes which can be placed on-top of step
 *   New Nodes such as candy cane 
 *   New crafts for the items
 *   More coloured Baubles
 *   More coloured presents
 *   bug fixes
 *   Some more stuff

HOW TO INSTALL?
------------------------------------------

1. extract the "christmas_craft_0.2" folder

2. Move the "christmas_craft" to the mod folder in your mintest game files

3. Start Minetest

4. under the "Singleplayer" tab press the Configure tab

5. enable "christmas_craft" then press Save

6. enjoy the mod

Configuration
------------------------------------------

Look into the world dir. there should be a file caled `christmas_craft.conf`.
The file contains the 2 folowing lines:

	enable_crafts = true
	enable_snowing = true
	
if you want to disable the craft recieps, because christmas is over, than you must just change the line `enable_crafts = true` into `enable_crafts = true`.
and if you dont like the snowy landscape just change `enable_snowing = true` to `enable_snowing = false`
thats all :)


OTHER STUFF
------------------------------------------

CHRISTMAS CRAFT MOD FOR MINETEST BY INFINATUM

thankyou for useing the mod

If you find any bugs post on the minetest forum;
https://forum.minetest.net/viewtopic.php?pid=118855#p118855


Find out more about the mod on our wiki;
https://bitbucket.org/kingarthursteam/christmas-craft/wiki/Home


Find newer devs of the mod on my site;
http://thatrspiserver.co.uk/Infinatum_Minetest/christmas_craft